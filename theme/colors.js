const colors = {
  white: '#fff',
  black: '#000',
  lightgray: '#dadfe7',
  gray: '#cccccc',
  darkgray: '#898989',
  blue: '#179aff',
  red: '#ff4d4d',
  lightred: '#ff6161',
  lightblue: '#42adff'
};

export default colors;