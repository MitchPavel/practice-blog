import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import styled from '@emotion/styled';
import Flex from '../components/primitives/Flex';
import Layout from '../components/Layout';
import Users from '../services/firestore/users';
import Auth from '../services/auth';
import log from '../services/log';
import { BlueButton } from '../components/buttons';
import { Column } from '../components/primitives/Flex';

const HorizontallyCentered = styled(Flex)`
  width: 100%;
  justify-content: center;
  margin-top: 20px;
`;

const Label = styled.label`
  display: block;
  margin-bottom: 8px;
`;

const Input = styled.input`
  display: block;
  margin-bottom: 5px;
  outline: none;
  border: none;
  border-radius: 3px;
  padding: 3px;
`;

const Account = () => {
  const router = useRouter();

  const [user, setUser] = useState({});
  Auth.listener(
    (user) => {
      if (user) {
        setUser(user);
      }
    },
    () => {
      setUser(null);
    },
  );
  useEffect(() => {
    if (!user) {
      router.push('/');
    }
  }, [user]);

  const [userData, setUserData] = useState({});
  useEffect(() => {
    if (user) {
      Users.get(user.email)
        .then(setUserData)
        .catch(log.error);
    }
  }, [user]);

  return (
    <Layout>
      <HorizontallyCentered>
        <Column>
          <Flex>
            <Column>
              <Label for="email">Email: </Label>
              <Label for="username">Username: </Label>
              <Label for="blog-title">Blog Title: </Label>
            </Column>
            <Column>
              <Input name="email" type="text" placeholder={userData.id} />
              <Input name="username" type="text" placeholder={userData.username} />
              <Input name="blog-title" type="text" placeholder={userData.blogTitle} />
            </Column>
          </Flex>
          <BlueButton>Update</BlueButton>
        </Column>
      </HorizontallyCentered>
    </Layout>
  );
};

export default Account;