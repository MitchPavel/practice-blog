import React from 'react';
import Layout from '../components/Layout';
import AllPosts from '../components/AllPosts';

export default function Home() {
  return (
    <Layout>
      <AllPosts />
    </Layout>
  );
};
