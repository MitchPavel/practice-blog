import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import moment from 'moment';
import { isNil } from 'ramda';
import styled from '@emotion/styled';
import Layout from '../../components/Layout';
import Auth from '../../services/auth';
import Users from '../../services/firestore/users';
import Posts from '../../services/firestore/posts';
import log from '../../services/log';
import { RedButton, BlueButton } from '../../components/buttons';
import { Row } from '../../components/primitives/Flex';
import { H2, P } from '../../components/primitives/text';
import colors from '../../theme/colors';

const Container = styled.div`
  width: 100%;
  padding: 30px;
`;

const ButtonContainer = styled(Row)`
  height: 25px;
  width: 125px;
  justify-content: space-between;
`;

const Meta = styled(P)`
  color: ${colors.darkgray};
  font-style: italic;
  text-align: center;
  margin-bottom: 30px;
`;

function formatTime(timestamp) {
  return moment(timestamp).format('M/D/YY h:mma');
}

const Post = () => {
  const router = useRouter();

  const [userId, setUserId] = useState(null);
  const [postId, setPostId] = useState(null);
  useEffect(() => {
    const { userId, postId } = router.query;
    setUserId(userId);
    setPostId(postId);
  }, []);

  const [user, setUser] = useState({});
  const [post, setPost] = useState({});
  useEffect(() => {
    if (isNil(userId) || isNil(postId)) { return; }

    Users.get(userId)
      .then(setUser)
      .catch(log.error);

    Posts.get(postId)
      .then(setPost)
      .catch(log.error);
  }, [userId, postId]);

  const { title, timestamp, content } = post;

  const deletePost = (e) => {
    Posts.delete(postId)
      .then(log.info)
      .then(() => router.push(`/${userId}`))
      .catch(log.error);
  };

  const editPost = (e) => { };

  return (
    <Layout title={user.blogTitle} heading={user.blogTitle}>
      <Container>
        <H2 style={{textAlign: 'center'}}>{title}</H2>
        <Meta>{user.username} - {formatTime(timestamp)}</Meta>
          {Auth.isOwner(userId) && (
            <ButtonContainer>
              <BlueButton onClick={editPost}>Edit</BlueButton>
              <RedButton onClick={deletePost}>Delete</RedButton>
            </ButtonContainer>
          )}
        <P>{content}</P>
      </Container>
    </Layout>
  );
}

export default Post;