import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { prop } from 'ramda';
import styled from '@emotion/styled';
import Layout from '../../components/Layout';
import BlogPostSummary from '../../components/BlogPostSummary';
import Users from '../../services/firestore/users';
import Posts from '../../services/firestore/posts';
import log from '../../services/log';
import Auth from '../../services/auth';
import BlogPostForm from '../../components/forms/BlogPostForm';
import { BlueButton } from '../../components/buttons';
import { H2 } from '../../components/primitives/text';
import PostListHeader from '../../components/PostListHeader';

const MainContainer = styled.div`
  width: 600px;
  margin: 0 auto;
  padding-bottom: 50px;
`;

const UserPosts = () => {
  const router = useRouter();

  const [userId, setUserId] = useState(null);
  useEffect(() => {
    const { userId } = router.query;
    setUserId(userId);
  });

  const [user, setUser] = useState({});
  const [posts, setPosts] = useState([]);
  const [refreshPosts, setRefreshPosts] = useState(false);
  useEffect(async () => {
    if (userId) {
      if (user) {
        await Users.get(userId)
          .then(setUser)
          .catch(log.error);
      }

      Posts.getAllForUser(userId)
        .then(setPosts)
        .catch(log.error);
    }

    setRefreshPosts(false);
  }, [userId, refreshPosts]);

  const [showForm, setShowForm] = useState(false);
  const toggleForm = () => {
    setShowForm(!showForm);
  };

  return (
    <Layout title={prop('blogTitle', user)} heading={prop('blogTitle', user)}>
      <MainContainer>
        <PostListHeader>
          {Auth.isOwner(userId)
            ? <>
                <H2>Your Recent Posts</H2>
                <BlueButton onClick={toggleForm}>New Post</BlueButton>
              </>
            : <>
                <H2>{prop('username', user)}'s Recent Posts</H2>
              </>
          }
        </PostListHeader>
        {posts.map(post => <BlogPostSummary key={prop('id', post)} post={post} refreshPosts={setRefreshPosts} />)}
      </MainContainer>
      {showForm && <BlogPostForm toggleForm={toggleForm} />}
    </Layout>
  );
}

export default UserPosts;
