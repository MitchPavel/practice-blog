import styled from '@emotion/styled';
import colors from '../../theme/colors';

const Button = styled.button`
  margin: 0 auto;
  border: none;
  border-radius: 5px;
  padding: 3px 6px;
  font-weight: bold;
  font-size: 16px;
  &:hover {
    background-color: ${colors.white};
    cursor: pointer;
  }
`;

export const BlueButton = styled(Button)`
  background-color: ${colors.blue};
  color: ${colors.white};
  &:hover {
    background-color: ${colors.lightblue};
  }
`;

export const WhiteButton = styled(Button)`
  background-color: ${colors.white};
  color: ${colors.blue};
  &:hover {
    background-color: ${colors.lightgray};
  }
`;

export const RedButton = styled(Button)`
  background-color: ${colors.red};
  color: ${colors.white};
  &:hover {
    background-color: ${colors.lightred};
  }
`;

export default Button;