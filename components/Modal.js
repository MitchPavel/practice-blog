import React from 'react';
import styled from '@emotion/styled';
import colors from '../theme/colors';

const Background = styled.div({
  position: 'fixed',
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  backgroundColor: 'rgba(0, 0, 0, 0.85)',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
});

const Container = styled.div({
  backgroundColor: colors.white,
  width: 600,
  padding: 25,
  borderRadius: 5,
});

const Modal = ({children}) => (
  <Background>
    <Container>{children}</Container>
  </Background>
);

export default Modal;