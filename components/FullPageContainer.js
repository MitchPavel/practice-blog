import React from 'react';
import colors from '../theme/colors';
import Flex from './primitives/Flex';

const FullPageContainer = ({ children }) => (
  <Flex style={{
    position: 'relative',
    minHeight: '100vh',
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: colors.lightgray,
  }}>
    {children}
  </Flex>
);

export default FullPageContainer;