import React, { useState } from 'react';
import Link from 'next/link';
import { prop } from 'ramda';
import Auth from '../services/auth';
import styled from '@emotion/styled';
import { FiLogIn, FiLogOut } from 'react-icons/fi';
import { RiAccountBoxLine } from 'react-icons/ri';
import colors from '../theme/colors';
import Flex from '../components/primitives/Flex';
import Button from '../components/buttons';

const Container = styled.div`
text-align: center;
border-bottom: 1px solid ${colors.gray};
background-color: ${colors.blue};
position: sticky;
top: 0;
display: flex;
justify-content: space-between;
padding: 0 10px;
`

const NavButton = styled(Button)`
  display: flex;
  align-items: center;
  margin-right: 10px;
  background-color: ${colors.blue};
  color: ${colors.black};
  font-weight: normal;
  &:hover {
    background-color: ${colors.white};
  }
  & > svg {
    margin-right: 5px;
  }
`;

const H1 = styled.h1`
  margin: 5px;
  &:hover {
    cursor: pointer;
  }
`;

const VerticallyCentered = styled(Flex)`
  align-items: center;
`;

const VerticallyCenteredContainer = styled(VerticallyCentered)`
  width: 200px;
  white-space: nowrap;
`;

const Welcome = ({ isAuthed, user }) => (
  <VerticallyCenteredContainer>
    {isAuthed
      ? <VerticallyCentered>
          Welcome, {prop('displayName', user)}!
        </VerticallyCentered>
      : <VerticallyCentered>
          Please sign in
        </VerticallyCentered>}
  </VerticallyCenteredContainer>
);

const Account = ({ isAuthed }) => (
  <VerticallyCenteredContainer>
    {isAuthed
      ? <>
        <Link href="/account" passHref>
          <NavButton>
            <RiAccountBoxLine/> Account
          </NavButton>
        </Link>
        <NavButton onClick={Auth.signout}>
          <FiLogOut /> Sign Out
        </NavButton>
      </>
      : <>
        <NavButton onClick={Auth.signin}>
          <FiLogIn /> Sign In
        </NavButton>
      </>}
  </VerticallyCenteredContainer>
);

const Header = ({ heading = "Blog Site" }) => {
  const [isAuthed, setIsAuthed] = useState(false);
  const [user, setUser] = useState(null);
  Auth.listener(
    (user) => {
      if (user) {
        setUser(user);
        setIsAuthed(true);
      }
    },
    () => {
      setIsAuthed(false);
    },
  );

  return (
    <Container id="header">
      <Welcome {...{ isAuthed, user }} />
      <Link href="/">
        <H1>{heading}</H1>
      </Link>
      <Account {...{ isAuthed }} />
    </Container>
  );
};

export default Header;
