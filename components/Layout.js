import React from 'react';
import Head from 'next/head';
import IfAuthed from '../components/firebase/IfAuthed';
import Header from '../components/Header';
import Footer from '../components/Footer';
import AuthorList from '../components/AuthorList';
import Flex, { Column } from '../components/primitives/Flex';
import styled from '@emotion/styled';
import colors from '../theme/colors';

const FullPageContainer = styled.div`
    flex-direction: column;
    justify-content: space-between;
    background-color: ${colors.lightgray};
`;

const Layout = ({
  children,
  title = "Bob, Mitch, & Matt's Blogtastic Adventure",
  iconUrl = "/favicon.ico",
  heading,
}) => (
  <FullPageContainer>
    <Head>
      <title>{title}</title>
      <link rel="icon" href={iconUrl} />
    </Head>

    <div>
      <Header heading={heading} />
      <IfAuthed>
        <Flex>
          <AuthorList />
          {children}
        </Flex>
        <Footer />
      </IfAuthed>
    </div>

  </FullPageContainer>
);

export default Layout;