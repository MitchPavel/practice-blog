import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import Flex from './primitives/Flex';
import colors from '../theme/colors';
import Users from '../services/firestore/users';
import log from '../services/log';
import { prop, path } from 'ramda';
import styled from '@emotion/styled';
import { H3 } from './primitives/text';

const HEADER_HEIGHT = 48;
const FOOTER_HEIGHT = 35;
const Container = styled(Flex)`
  flex-direction: column;
  min-width: 200px;
  min-height: calc(100vh - ${HEADER_HEIGHT + FOOTER_HEIGHT}px);
  background-color: ${colors.white};
  padding: 10px;
  padding-bottom: 0px;
`;

export default function AuthorList() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    Users.getAll()
      .then(setUsers)
      .catch(log.error);
  }, []);

  return (
    <Container id="author-list">
      <H3>Authors:</H3>
      {users.map(user => (
        <Link href={`/${prop('id', user)}`} passHref key={user.id}>
          <a>{user.username} - {user.posts.length}</a>
        </Link>
      ))}
    </Container>
  );
}