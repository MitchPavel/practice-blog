import React from 'react';
import { Formik, Form } from 'formik';
import { Label, TextInput, TextArea } from '../forms/fields';
import styled from '@emotion/styled';
import Modal from '../Modal';
import Flex from '../primitives/Flex';
import Auth from '../../services/auth';
import Posts from '../../services/firestore/posts';
import { prop } from 'ramda';
import log from '../../services/log';
import { BlueButton, WhiteButton } from '../../components/buttons';
import { H2 } from '../../components/primitives/text';

const ButtonContainer = styled(Flex)`
  justify-content: space-around;
  margin-top: 10px;
`;

const BlogPostForm = ({ toggleForm }) => {
  const submitForm = (values) => {
    const user = Auth.currentUser();
    if (user) {
      Posts.add({
        title: prop('title', values),
        timestamp: new Date(),
        authorId: prop('email', user),
        content: prop('content', values),
      }).then(log.info)
        .catch(log.error);
      toggleForm();
    }
  };

  return (
    <Modal>
      <H2>New Post</H2>
      <Formik
        initialValues={{
          title: '',
          content: '',
        }}
        onSubmit={submitForm}
      >
        <Form>
          <Label for="title">Title
            <TextInput
              id="title"
              name="title"
              placeholder="Random Thoughts"
            />
          </Label>

          <TextArea
            as="textarea"
            id="content"
            name="content"
            placeholder="Lately I've been thinking a lot about..."
            style={{ width: 550, height: 100 }}
          />

          <ButtonContainer>
            <BlueButton type="submit">Submit</BlueButton>
            <WhiteButton type="cancel" onClick={toggleForm}>Cancel</WhiteButton>
          </ButtonContainer>
        </Form>
      </Formik>
    </Modal>
  );
};

export default BlogPostForm;