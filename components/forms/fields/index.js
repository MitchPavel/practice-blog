import styled from '@emotion/styled';
import { Field } from 'formik';

export const Label = styled.label`
  display: block;
  margin-bottom: 8px;
`;

export const TextInput = styled(Field)`
  display: block;
  margin-bottom: 5px;
  outline: none;
  border: 1px solid lightgray;
  border-radius: 3px;
  padding: 3px;
  font-size: 1em;
  font-family: inherit;
`;

const TextAreaField = ({children, ...props}) => (
  <Field as="textarea" {...props}>
    {children}
  </Field>
);

export const TextArea = styled(TextAreaField)`
  display: block;
  margin-bottom: 5px;
  outline: none;
  border: 1px solid lightgray;
  border-radius: 3px;
  padding: 3px;
  font-size: 1em;
  font-family: inherit;
`;