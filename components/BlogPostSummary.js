import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { __, concat, gt, join, length, pipe, slice, split, unless } from 'ramda';
import styled from '@emotion/styled';
import Link from '../components/primitives/Link';
import colors from '../theme/colors';
import Users from '../services/firestore/users';
import log from '../services/log';
import { Row } from './primitives/Flex';
import { H3, P } from '../components/primitives/text';

const Container = styled.div`
  border: 1px solid ${colors.gray};
  border-radius: 5px;
  background-color: ${colors.white};
  margin: 10px auto;
  padding: 15px;
  max-width: 500px;
  width: 100%;
  &:hover {
    cursor: pointer;
    border: 1px solid ${colors.darkgray};
  }
`;

const HeaderContainer = styled(Row)`
  justify-content: space-between;
`;

const PREVIEW_LENGTH = 100;

const createPreview = pipe(
  slice(0, PREVIEW_LENGTH),
  split(' '),
  slice(0, -1),
  join(' '),
  concat(__, '...'),
);

const preview = unless(
  pipe(
    length,
    gt(PREVIEW_LENGTH),
  ),
  createPreview,
);

function formatTime(timestamp) {
  return moment(timestamp).format('M/D/YY h:mma')
}

const BlogPostSummary = ({ post, setRefreshPosts }) => {
  const { id, title, authorId, timestamp, content } = post;
  if (!content) {
    return null;
  }

  const [username, setUsername] = useState(undefined);
  useEffect(() => {
    Users.getUsername(authorId)
      .then(setUsername)
      .catch(log.error);
  }, [post]);

  return (
    <Link href={`/${authorId}/${id}`} passHref>
      <Container>
        <HeaderContainer>
          <H3>{title}</H3>
        </HeaderContainer>
        <P>{username} - {formatTime(timestamp)}</P>
        <P>{preview(content)}</P>
      </Container>
    </Link>
  );
};

export default BlogPostSummary;