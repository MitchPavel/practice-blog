import React, {useEffect, useState} from 'react';
import { prop } from 'ramda';
import styled from '@emotion/styled';
import BlogPostSummary from './BlogPostSummary';
import Posts from '../services/firestore/posts';
import log from '../services/log';
import { H2 } from '../components/primitives/text';
import PostListHeader from '../components/PostListHeader';

const Container = styled.div`
  width: 100%;
  padding-bottom: 50px;
`;

const LeftH2 = styled(H2)`
  text-align: left;
`;

export default function AllPosts() {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    Posts.getAll()
      .then(setPosts)
      .catch(log.error);
  }, []);

  return (
    <Container>
      <PostListHeader style={{ maxWidth: 500, margin: '30px auto 10px auto' }}>
        <LeftH2 style={{ textAlign: 'left' }}>Recent Posts</LeftH2>
      </PostListHeader>
      {posts.map(post => <BlogPostSummary key={prop('id', post)} post={post} />)}
    </Container>
  );
}