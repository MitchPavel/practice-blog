import React, { useState } from 'react';
import Auth from '../../services/auth';

const IfAuthed = ({children}) => {
  const [isAuthed, setIsAuthed] = useState(false);
  Auth.listener(
    (user) => {
      if (user) {
        setIsAuthed(true);
      }
    },
    () => {
      setIsAuthed(false);
    },
  );

  return (
    <>
      {isAuthed ? children : null}
    </>
  );
};

export default IfAuthed;