import styled from '@emotion/styled';
import { Row } from './primitives/Flex';

const PostListHeader = styled(Row)`
align-items: center;
justify-content: space-between;
max-width: 500px;
margin: 30px auto 10px auto;
`;

export default PostListHeader;