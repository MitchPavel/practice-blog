import React from 'react';
import NextLink from 'next/link';

const Link = ({children, ...props}) => (
  <NextLink {...props}>
    <a>{children}</a>
  </NextLink>
);

export default Link;