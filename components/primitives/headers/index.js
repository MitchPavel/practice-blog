import styled from '@emotion/styled';

export const H1 = styled.h1`
  margin: 0 5px;
`;

export const H2 = styled.h2`
  margin: 0 5px;
  text-decoration: underline;
`;

export const H3 = styled.h3`
  margin: 0 2px;
  font-style: italic;
`;