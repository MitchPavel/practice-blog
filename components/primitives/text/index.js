import styled from '@emotion/styled';

export const H2 = styled.h2`
  margin: 0 5px;
  text-decoration: underline;
`;

export const H3 = styled.h3`
  margin: 2px;
  margin-bottom: 7px;
  font-style: italic;
`;

export const P = styled.p`
  margin: 8px;
`;


/*
Other things to maybe add:
- section
- article - use for full post content and comments
- aside?
- span?
*/