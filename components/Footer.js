import React from 'react';
import styled from '@emotion/styled';
import colors from '../theme/colors';
import Flex from './primitives/Flex';
import { P } from '../components/primitives/text';

const Container = styled(Flex)`
      display: flex;
      justify-content: center;
      border-top: 1px solid ${colors.gray};
      width: 100%;
      background-color: ${colors.blue};
`;

const Footer = () => (
  <Container id="footer">
    <P>Copyright 2021</P>
  </Container>
);

export default Footer;