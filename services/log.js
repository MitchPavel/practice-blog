const log = (function() {
  return {
    info() {
      console.log(...arguments);
    },
    error() {
      console.error(...arguments);
    },
    warn() {
      console.warn(...arguments);
    },
  };
})();

export default log;