import firebase from '../../config/firebase';
import 'firebase/auth';
import { prop } from 'ramda';
import log from '../log';

const provider = new firebase.auth.GoogleAuthProvider();

const authHandler = (user) => log.info(`${user.displayName || user.email} currently signed in.`);
const noAuthHandler = () => log.info('No user is currently signed in.');
const listener = (onAuth=authHandler, onNoAuth=noAuthHandler) => {
  firebase.auth().onAuthStateChanged(user => {
    if (user) {
      onAuth(user);
    } else {
      onNoAuth();
    }
  });
};

const signinSuccess = (result) => log.info(`${result.user.displayName || result.user.email} successfully signed in.`);
const signinFailure = (error) => log.info(error.message);
const signin = (onSuccess=signinSuccess, onFailure=signinFailure) => {
  firebase.auth()
    .signInWithPopup(provider)
    .then(onSuccess)
    .catch(onFailure);
};

const signoutSuccess = () => log.info('User successfully logged out.');
const signoutFailure = (error) => log.info(error.message);
const signout = (onSuccess=signoutSuccess, onFailure=signoutFailure) => {
  firebase.auth().signOut()
    .then(onSuccess)
    .catch(onFailure);
};

const currentUser = () => {
  return firebase.auth().currentUser;
};

const isOwner = (ownerId) => {
  const user = Auth.currentUser();
  if (user) {
    return ownerId === prop('email', user);
  }
  return false;
}

const Auth = {
  signin,
  signout,
  listener,
  currentUser,
  isOwner,
};

export default Auth;