import firestore from './index';
import { usersCollection } from './users';
import { commentsCollection } from './comments';
import {
  applySpec,
  clone,
  forEachObjIndexed,
  is,
  isEmpty,
  pipe,
  prop,
  where
} from 'ramda';

const db = firestore();
const Timestamp = firestore.Timestamp;
const FieldValue = firestore.FieldValue;
export const postsCollection = db.collection('posts');

// comments field cannot be added or updated directly
const spec = {
  title: is(String),
  timestamp: is(Date),
  authorId: is(String),
  content: is(String),
};

const isValid = where(spec);

const initializePayload = (data) => {
  const payload = clone(data);
  const removeEmptyValues = (value, key) => isEmpty(value) && delete payload[key];
  const sanitizedPayload = forEachObjIndexed(removeEmptyValues, payload);
  return applySpec({
    title: prop('title'),
    timestamp: pipe(
      prop('timestamp'),
      Timestamp.fromDate
    ),
    authorId: prop('authorId'),
    content: prop('content'),
  })(sanitizedPayload);
};

const createEmptyPost = async () => {
  const postRes = await postsCollection.add({ empty: true });
  return postRes.id;
};

const clearAnyEmptyPosts = async () => {
  return await db.runTransaction(async (t) => {
    const emptyPosts = await postsCollection.where('empty', '==', true).get();
    emptyPosts.forEach(post => {
      const postRef = postsCollection.doc(post.id);
      t.delete(postRef);
    });
    return 'All empty posts have been deleted.';
  });
};

// Add data to empty post and update user doc within batch write.
const updatePostAndAddToUser = async (postId, payload) => {
  const batch = db.batch();
  const postRef = postsCollection.doc(postId);
  batch.set(postRef, payload);
  const userId = payload.authorId;
  const userRef = usersCollection.doc(userId);
  const user = await userRef.get();
  if (!user.exists) {
    throw Error(`User ${userId} does not exist.`);
  }
  batch.update(userRef, {
    posts: FieldValue.arrayUnion(postId),
  });
  return batch.commit();
};

const Posts = {
  add: async (data) => {
    if (isValid(data)) {
      try {
        const postId = await createEmptyPost();
        const payload = initializePayload(data);
        const res = await updatePostAndAddToUser(postId, payload);
        return res || `Post ${postId} added for user ${payload.authorId}.`;
      } catch (err) {
        clearAnyEmptyPosts();
        return err;
      }
    } else {
      return 'Invalid data.';
    }
  },

  update: async (id, data) => {
    const postRef = postsCollection.doc(id);
    const postDoc = await postRef.get();
    if (!postDoc.exists) {
      return `The post ${id} does not exist.`;
    }
    const payload = applySpec({ content: prop('content') })(data);
    const res = await postRef.update(payload);
    return res || `Updated post ${id}.`;
  },

  delete: async (id) => {
    const postRef = postsCollection.doc(id);

    const res = await db.runTransaction(async (t) => {
      const post = await t.get(postRef);
      if (!post.exists) {
        return `The post ${id} does not exist.`;
      }
      t.delete(postRef);
      const postData = post.data();
      const userId = postData.authorId;
      const userRef = usersCollection.doc(userId);
      t.update(userRef, {
        posts: FieldValue.arrayRemove(id),
      });
      const comments = postData.comments;
      if (comments) {
        comments.forEach(commentId => {
          const commentRef = commentsCollection.doc(commentId);
          t.delete(commentRef);
        });
      }
      return `Deleted post ${id}.`
    });

    return res;
  },

  get: async (id) => {
    const postRef = postsCollection.doc(id);
    const postDoc = await postRef.get();
    if (!postDoc.exists) {
      return `The post ${id} does not exist.`;
    }
    const post = await postDoc.data();
    post.id = id;
    post.timestamp = post.timestamp.toDate();
    return post;
  },

  getAllForUser: async (userId) => {
    const user = await usersCollection.doc(userId).get();
    const posts = user.data().posts;
    const pendingPosts = posts.map(async post => await Posts.get(post));
    return Promise.all(pendingPosts);
  },

  getAll: async () => {
    const posts = await postsCollection.get();
    const list = [];
    posts.forEach(p => list.push(p));
    return list.map(post => {
      const data = post.data();
      return {
        id: post.id,
        ...data,
        timestamp: data.timestamp.toDate()
      };
    });
  },
};

export default Posts;