import firestore from './index';
import { postsCollection } from './posts';
import {
  applySpec,
  clone,
  forEachObjIndexed,
  is,
  isEmpty,
  pipe,
  prop,
  where
} from 'ramda';

const db = firestore();
const Timestamp = firestore.Timestamp;
const FieldValue = firestore.FieldValue;
export const commentsCollection = db.collection('comments');

const spec = {
  authorId: is(String),
  postId: is(String),
  timestamp: is(Date),
  content: is(String),
};

const isValid = where(spec);

const initializePayload = (data) => {
  const payload = clone(data);
  const removeEmptyValues = (value, key) => isEmpty(value) && delete payload[key];
  const sanitizedPayload = forEachObjIndexed(removeEmptyValues, payload);
  return applySpec({
    authorId: prop('authorId'),
    postId: prop('postId'),
    timestamp: pipe(
      prop('timestamp'),
      Timestamp.fromDate
    ),
    content: prop('content'),
  })(sanitizedPayload);
};

const createEmptyComment = async () => {
  const commentRes = await commentsCollection.add({ empty: true });
  return commentRes.id;
};

const deleteCommentIfEmpty = async (id) => {
  const commentRef = commentsCollection.doc(id);
  await db.runTransaction(async (t) => {
    const comment = await t.get(commentRef);
    if (comment.data().empty) {
      t.delete(commentRef);
    }
  });
};

const clearAnyEmptyComments = async () => {
  return await db.runTransaction(async (t) => {
    const emptyComments = await commentsCollection.where('empty', '==', true).get();
    emptyComments.forEach(comment => {
      const commentRef = commentsCollection.doc(comment.id);
      t.delete(commentRef);
    });
    return 'All empty comments have been deleted.';
  });
};

// Add data to empty comment and update post doc within batch write.
const updateCommentAndAddToPost = (commentId, payload) => {
  const batch = db.batch();
  const commentRef = commentsCollection.doc(commentId);
  batch.set(commentRef, payload);
  const postId = payload.postId;
  const postRef = postsCollection.doc(postId);
  batch.update(postRef, {
    comments: FieldValue.arrayUnion(commentId),
  });
  return batch.commit();
};

const Comments = {
  add: async (data) =>  {
    if (isValid(data)) {
      try {
        const commentId = await createEmptyComment();
        const payload = initializePayload(data);
        const res = await updateCommentAndAddToPost(commentId, payload);
        return res || `Comment ${commentId} added to post ${payload.postId}.`;
      } catch (err) {
        clearAnyEmptyComments();
      }
    } else {
      return 'Invalid data.';
    }
  },

  update: async (id, data) => {
    const commentRef = commentsCollection.doc(id);
    const commentDoc = await commentRef.get();
    if (!commentDoc.exists) {
      return `The comment ${id} does not exist.`;
    }
    const payload = applySpec({ content: prop('content') })(data);
    const res = await commentRef.update(payload);
    return res || `Updated comment ${id}.`;
  },

  delete: async (id) => {
    const commentRef = commentsCollection.doc(id);

    const res = await db.runTransaction(async (t) => {
      const comment = await t.get(commentRef);
      if (!comment.exists) {
        return `The comment ${id} does not exist.`;
      }
      t.delete(commentRef);
      const commentData = comment.data();
      const postId = commentData.postId;
      const postRef = postsCollection.doc(postId);
      t.update(postRef, {
        comments: FieldValue.arrayRemove(id),
      });
      return `Deleted comment ${id}.`
    });

    return res;
  },

  get: async (id) => {
    const commentRef = commentsCollection.doc(id);
    const commentDoc = await commentRef.get();
    if (!commentDoc.exists) {
      return `The comment ${id} does not exist.`;
    }
    const comment = await commentDoc.data();
    comment.id = id;
    comment.timestamp = comment.timestamp.toDate();
    return comment;
  },

  getAllForPost: async (postId) => {
    const post = await postsCollection.doc(postId).get();
    const comments = post.data().comments;
    const pendingComments = comments.map(async comment => await Comments.get(comment));
    return await Promise.all(pendingComments);
  },
};

export default Comments;