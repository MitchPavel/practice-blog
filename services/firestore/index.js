import firebase from '../../config/firebase';
import 'firebase/firestore';

export const firestore = firebase.firestore;

export default firestore;