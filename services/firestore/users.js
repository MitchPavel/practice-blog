import firestore from './index';
import { postsCollection } from './posts';
import { commentsCollection } from './comments';
import log from '../log';
import {
  applySpec,
  clone,
  forEachObjIndexed,
  isEmpty,
  is,
  keys,
  pick,
  prop,
  propOr,
  where
} from 'ramda';

const db = firestore();
export const usersCollection = db.collection('users');

// posts field cannot be added or updated directly
const spec = {
  username: is(String),
  blogTitle: is(String),
};

const isValid = where(spec);

const isUsernameTaken = async (username) => {
  const user = await usersCollection.where('username', '==', username).get();
  return !user.empty;
};

const initializePayload = (data) => {
  const payload = clone(data);
  const removeEmptyValues = (value, key) => isEmpty(value) && delete payload[key];
  const sanitizedPayload = forEachObjIndexed(removeEmptyValues, payload);
  return applySpec({
    username: prop('username'),
    blogTitle: propOr(`${sanitizedPayload.username}'s blog`, 'blogTitle'),
  })(sanitizedPayload);
};

// user id is email address
const Users = {
  add: async (id, data) => {
    const userRef = usersCollection.doc(id);
    const userDoc = await userRef.get();
    if (userDoc.exists) {
      return `User already exists with the email ${id}.`;
    }
    if (await isUsernameTaken(data.username)) {
      return `The username '${data.username}' is not available.`;
    }
    const payload = initializePayload(data);
    if (isValid(payload)) {
      const res = await userRef.set(payload);
      return res || `Added user ${id}.`;
    } else {
      return 'Invalid data.';
    }
  },

  // Ignores any fields not in spec.
  update: async (id, data) => {
    const userRef = usersCollection.doc(id);
    const userDoc = await userRef.get();
    if (!userDoc.exists) {
      return `The user ${id} does not exist.`;
    }
    const payload = pick(keys(spec), data);
    const res = await userRef.update(payload);
    return res || `Updated user ${id}.`;
  },

  delete: async (id) => {
    const refs = [];
    const userRef = usersCollection.doc(id);
    refs.push(userRef);

    const res = await db.runTransaction(async (t) => {
      const user = await t.get(userRef);
      if (!user.exists) {
        return `The user ${id} does not exist.`;
      }
      
      const userData = user.data();
      const posts = propOr([], 'posts')(userData);
      // add post refs to refs array
      for (let i = 0; i < posts.length; i += 1) {
        const postId = posts[i];

        const postRef = postsCollection.doc(postId);
        refs.push(postRef);

        const post = await t.get(postRef);
        const postData = post.data();
        const comments = propOr([], 'comments')(postData);
        // add comment refs to refs array
        for (let j = 0; j < comments.length; j += 1) {
          const commentId = comments[j];
          const commentRef = commentsCollection.doc(commentId);
          refs.push(commentRef);
        }
      }

      refs.forEach(ref => t.delete(ref));
      return `Deleted user ${id}.`
    });

    return res;
  },

  getUsername: async (id) => {
    const userRef = usersCollection.doc(id);
    const userDoc = await userRef.get();
    if (!userDoc.exists) {
      return `The user ${id} does not exist.`;
    }
    return userDoc.data().username;
  },

  get: async (id) => {
    const userRef = usersCollection.doc(id);
    const userDoc = await userRef.get();
    if (!userDoc.exists) {
      return `The user ${id} does not exist.`;
    }
    const user = userDoc.data();
    user.id = id;
    return user;
  },

  getAll: async () => {
    const users = await usersCollection.get();
    const list = [];
    users.forEach(u => list.push(u));
    return list.map((user, i) => {
      const data = user.data();
      return { id: user.id, ...data };
    });
  },
};

export default Users;